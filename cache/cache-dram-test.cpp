#include <string>
#include <iostream>
#include <fstream>
#include "cache.h"
#include "cache-dram-test.h"

using namespace DRAMSim;


int tick = 0;
bool ready = false;

/* new callback */
void simple_read(Request *req)
{
  if (verbose)
    std::cout << "Read complete for " << std::hex << req->addr << std::endl;
  ready = true;
}

void simple_write(Request *req)
{
  if (verbose)
    std::cout << "Write complete for " << std::hex << req->addr << std::endl;
  ready = true;
}

void simple_fetch(Request *req)
{
  if (verbose)
    std::cout << "Fetch complete for " << std::hex << req->addr << std::endl;
  ready = true;
}


/* callback functors */
void some_object::read_complete(unsigned id, uint64_t address, uint64_t clock_cycle)
{
	printf("[Callback] read complete: %d 0x%lx cycle=%lu\n", id, address, clock_cycle);
	ready = true;
}

void some_object::write_complete(unsigned id, uint64_t address, uint64_t clock_cycle)
{
	printf("[Callback] write complete: %d 0x%lx cycle=%lu\n", id, address, clock_cycle);
	ready = true;
}

/* FIXME: this may be broken, currently */
void power_callback(double a, double b, double c, double d)
{
//	printf("power callback: %0.3f, %0.3f, %0.3f, %0.3f\n",a,b,c,d);
}

int some_object::add_one_and_run(MultiChannelMemorySystem *mem, uint64_t addr)
{

	/* create a transaction and add it */
	bool isWrite = false; 
	mem->addTransaction(isWrite, addr);

	// send a read to channel 1 on the same cycle 
	addr = 1LL<<33 | addr; 
	mem->addTransaction(isWrite, addr);

	for (int i=0; i<5; i++)
	{
		mem->update();
	}

	/* add another some time in the future */

	// send a write to channel 0 
	addr = 0x900012; 
	isWrite = true; 
	mem->addTransaction(isWrite, addr);
	

	/* do a bunch of updates (i.e. clocks) -- at some point the callback will fire */
	for (int i=0; i<45; i++)
	{
		mem->update();
	}

	/* get a nice summary of this epoch */
	mem->printStats(true);

	return 0;
}



void simple_cb(Request *req)
{
  std::cout << "Data Ready for Request(" << req << ", addr=" << req->addr << ")" << std::endl;
}


/* TODO

   extend cache to use dram interface on miss
   dump stats
   fix off by 1 error on hit

 */




int main(int argc, char **argv)
{
  some_object obj;
  TransactionCompleteCB *read_cb = new Callback<some_object, void, unsigned, uint64_t, uint64_t>(&obj, &some_object::read_complete);
  TransactionCompleteCB *write_cb = new Callback<some_object, void, unsigned, uint64_t, uint64_t>(&obj, &some_object::write_complete);

  Cache *il1, *dl1, *l2;
  int idx;

  tick_t now = 0;

  int block_size = 64;
  int il1_capacity, il1_assoc,  il1_lat;
  int dl1_capacity, dl1_assoc,  dl1_lat;
  int l2_capacity, l2_assoc,  l2_lat;

  il1_capacity = 8 * 1024;
  il1_assoc = 4;
  il1_lat = 1;

  dl1_capacity = 8 * 1024;
  dl1_assoc = 4;
  dl1_lat = 1;

  l2_capacity = 32 * 1024;
  l2_assoc = 8;
  l2_lat = 12;

  il1 = new Cache("IL1", il1_assoc, il1_capacity, block_size, il1_lat);
  dl1 = new Cache("DL1", dl1_assoc, dl1_capacity, block_size, dl1_lat);
  l2 = new Cache("L2", l2_assoc, l2_capacity, block_size, l2_lat);

  // 	MultiChannelMemorySystem *getMemorySystemInstance(const string &dev, const string &sys, const string &pwd, const string &trc, unsigned megsOfMemory, std::string *visfilename=NULL);
  MultiChannelMemorySystem *mem = getMemorySystemInstance("basic-mem.ini", "system.ini", ".", "example_app", 16384); 

  // provide call backs for hit, miss, fill...initialized to NULL
  
  il1->connect_next(l2);    // registers connections both ways
  dl1->connect_next(l2);    // registers connections both ways
  l2->connect_dram(mem);


  std::ifstream input_file;
  char *trace_fname = NULL;

  if (argc > 1) {
    trace_fname = argv[1];
    input_file.open(trace_fname);
  }
  std::istream& trace = (trace_fname) ? input_file : cin;

  std::string cmd;
  maddr_t addr;
  while (trace >> cmd >> std::hex >> addr) {

    if (cmd == "R" || cmd == "r") {
      auto req = dl1->access(CACHE_CMD_READ, addr, simple_read);
      std::cout << "[" << std::dec << tick << "]\tREAD  at 0x" << std::hex << addr <<std::endl;
    }
    else if (cmd == "W" || cmd == "w") {
      auto req = dl1->access(CACHE_CMD_WRITE, addr, simple_write);
      std::cout << "[" << std::dec << tick << "]\tWRITE at 0x" << std::hex << addr << std::endl;
    }
    else if (cmd == "f" || cmd == "F") {
      auto req = il1->access(CACHE_CMD_READ, addr, simple_fetch);
      std::cout << "[" << std::dec << tick << "]\tFETCH at 0x" << std::hex << addr << std::endl;
    }
    else {
      std::cerr << "Unexpected command '" << cmd << "'" << std::endl;
      exit(-1);
    }


    ready = false;
    do {
      il1->update();
      dl1->update();
      l2->update();
      mem->update();
      tick++;
    } while (!ready);
  }

  std::cout << "Simulation finished @" << dec << tick << std::endl;

  /* 
     Make pervasive use of callbacks to handle all the accesses in the system...
     Need to rewrite cache functions so that they query the DRAM system...

  */


  il1->dump_stats();
  dl1->dump_stats();
  l2->dump_stats();

  //fprintf(stdout, "miss_rate = %f\n", ((double) dl1->get_overall_misses() / dl1->get_accesses()));
  /*  
  fprintf(stdout, "l2_miss_rate = %f\n", ((double) cache_overall_misses(l2) / l2->access_count));
  fprintf(stdout, "avg_latency = %f\n",  (double)(lat_sum) / tc->access_count); */
}


