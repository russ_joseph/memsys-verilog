#include  <iverilog/vpi_user.h>
//#include  <iverilog/veriuser.h>
#include "cache.h"


static dram_t* main_mem;
static cache_t* icache;
static cache_t* dcache;
static cache_t* l2cache;

static int icache_access_compiletf(char* user_data)
{
  return 0;
}

static int dcache_access_compiletf(char* user_data)
{
  return 0;
}


//static void cache_access_getargs(vpiHandle systfref, maddr_t *addr, cache_cmd_t *cmd, tick_t *now)
static void cache_access_getargs(vpiHandle systfref,  int *addr, int  *cmd, int *now)
{
  vpiHandle argh, args_iter;
  struct t_vpi_value argval;

  args_iter = vpi_iterate(vpiArgument, systfref);  

  // Grab the value of the first argument
  argh = vpi_scan(args_iter);
  argval.format = vpiIntVal;
  vpi_get_value(argh, &argval);
  *addr = argval.value.integer;

  // Grab the value of the second argument
  argh = vpi_scan(args_iter);
  vpi_get_value(argh, &argval);
  *cmd = argval.value.integer;

  // Grab the value of the third argument
  argh = vpi_scan(args_iter);
  vpi_get_value(argh, &argval);
  *now = argval.value.integer;

  vpi_free_object(args_iter);
}


static void cache_access_return(vpiHandle systfref, tick_t latency)
{
  struct t_vpi_value argval;

  argval.value.integer = latency;
  argval.format = vpiIntVal;
  vpi_put_value(systfref, &argval, NULL, vpiNoDelay);
}

static int icache_access_calltf(char* user_data)
{
  vpiHandle systfref;
  int addr, cmd, now, lat; // these will be casted to proper types

  systfref = vpi_handle(vpiSysTfCall, NULL); // Get handle to task 
  cache_access_getargs(systfref, &addr, &cmd, &now); // Get arguments

  lat = cache_access(icache, addr, cmd, now);
  vpi_printf("icache_access(%x, %x, %x)\n", addr, cmd, now);

  cache_access_return(systfref, lat); // Return latency

  return 0;
}

static int dcache_access_calltf(char* user_data)
{
  vpiHandle systfref;
  int addr, cmd, now, lat; // these will be casted to proper types

  systfref = vpi_handle(vpiSysTfCall, NULL); // Get handle to task 
  cache_access_getargs(systfref, &addr, &cmd, &now); // Get arguments
  
  lat = cache_access(dcache, addr, cmd, now);
  vpi_printf("dcache_access(%x, %x, %x)\n", addr, cmd, now);

  cache_access_return(systfref, lat); // Return latency

  return 0;
}



void cache_access_register()
{
  s_vpi_systf_data icache_access_tf_data, dcache_access_tf_data;

  icache_access_tf_data.type      = vpiSysFunc;
  icache_access_tf_data.tfname    = "$icache_access";
  icache_access_tf_data.calltf    = icache_access_calltf;
  icache_access_tf_data.compiletf = icache_access_compiletf;
  icache_access_tf_data.sizetf    = 0;
  icache_access_tf_data.user_data = 0;
  vpi_register_systf(&icache_access_tf_data);

  dcache_access_tf_data.type      = vpiSysFunc;
  dcache_access_tf_data.tfname    = "$dcache_access";
  dcache_access_tf_data.calltf    = dcache_access_calltf;
  dcache_access_tf_data.compiletf = dcache_access_compiletf;
  dcache_access_tf_data.sizetf    = 0;
  dcache_access_tf_data.user_data = 0;
  vpi_register_systf(&dcache_access_tf_data);

  main_mem = dram_alloc("main_mem", 200);
  l2cache = (cache_t*) cache_alloc("ul2", 8, 262144, 64, 12,  NULL, main_mem);
  icache = (cache_t*) cache_alloc("il1", 4, 16384, 64, 1, l2cache, NULL);
  dcache = (cache_t*) cache_alloc("dl1", 4, 16384, 64, 1, l2cache, NULL);
}

void (*vlog_startup_routines[])() = {
  cache_access_register,   
  0
};
