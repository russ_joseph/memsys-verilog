#include <memory.h>
#include <string.h>
#include <malloc.h>
#include <assert.h>

#include "cache.h"

bool verbose = false;

static int my_log2(int i) {
  int result = 0;
  while (i >>= 1) ++result;
  return result;
}


/* 

   reqTable - tracks requests related to this cache
   eventQ - tracks lookups related to this cache


   access(cb) - on top level access:
                   create new req w/ cb and place in reqTable
                   insert lookup into eventQ:
                          on hit event:
                                 remove req from reqTable
				 use req's cb
			  on miss event:
                                 initiate access to next level w/ remoteDataReady as cb
                                 
  completeRemoteReq - remove req from reqTable
                      use req's cb

  update()  - query eventQ
              increment now

 */


Cache::Cache(std::string name, int assoc, int capacity, int block_size, int latency) :
  name(name),
  assoc(assoc),
  block_size(block_size),
  latency(latency),
  access_count(0),
  hit_counts(CACHE_CMD_COUNT),
  miss_counts(CACHE_CMD_COUNT)  
{
  int set_index, way_index;

  assert(assoc > 0);
  assert((capacity & (capacity-1)) == 0);
  assert((block_size & (block_size-1)) == 0);

  this->n_sets = capacity / (assoc * block_size);

  this->block_shift = my_log2(block_size);
  this->block_mask = ~((1 << my_log2(block_size)) - 1);
  this->set_mask = this->n_sets - 1;
  this->offset_mask = ~this->block_mask;


  this->blocks.resize(n_sets);
  for(set_index = 0; set_index < this->n_sets; set_index++) {
    this->blocks[set_index].resize(assoc);
    for(way_index = 0; way_index < assoc; way_index++)
      this->blocks[set_index][way_index] = new Block();
  }

}

void Cache::lru_reorder(Block *blk)
{
  int set_index = this->findset(blk->block_addr);

  for(auto it = blocks[set_index].begin(); it != blocks[set_index].end(); it++) {
    auto currBlk = *it;
    if (blk == currBlk) {
      blocks[set_index].erase(it);
      blocks[set_index].insert( blocks[set_index].begin(), currBlk);
    }
  }
}


Block* Cache::probe(maddr_t addr) 
{
  int set_index = this->findset(addr);
  Block *way_blk, *blk = NULL;
  int way_index;

  assert(set_index < this->n_sets);


  for(way_index = 0; way_index < assoc; way_index++) {
    way_blk = this->blocks[set_index][way_index];
    if (way_blk->block_addr == (addr & this->block_mask) && way_blk->valid) {    
      blk = way_blk;
      break;
    }
  }

  return blk;
}

/*
  Cache::handle_miss() - pick victim, schedule writeback (if needed), fetch replacement
 */
Block* Cache::handle_miss(cache_cmd_t cmd, maddr_t addr)
{
  maddr_t block_addr = addr & this->block_mask;
  int set_index = this->findset(block_addr);
  Block *blk;
  int lat;

  // no writeback for now...

  blk = this->blocks[set_index].back();

  blk->valid = true;
  blk->dirty = (cmd == CACHE_CMD_WRITE);
  blk->block_addr = block_addr;

  return blk;
}

void Cache::connect_next(Cache *lower)
{
  this->next_level = lower;
}

void empty_cb(double a, double b, double c, double d)
{
}

RequestPtr Cache::find_mem_request(uint64_t address)
{
  RequestPtr req;

  for(auto req_it = memReqs.begin(); req_it != memReqs.end(); req_it++)
    if ((*req_it)->addr == address) {
      req = *req_it;
      break;
    }
  

  assert(req);
  return req;
}

void Cache::mem_read_complete(unsigned id, uint64_t address, uint64_t clock_cycle)
{
  if (verbose)
    std::cout << "READ complete!!!!!" << std::endl;
  
  auto req = find_mem_request(address);
  req->complete = true;

}

void Cache::mem_write_complete(unsigned id, uint64_t address, uint64_t clock_cycle)
{
  if (verbose)
    std::cout << "WRITE complete!!!!!" << std::endl;

  auto req = find_mem_request(address);
  req->complete = true;

}

void Cache::connect_dram(DRAMSim::MultiChannelMemorySystem* mem)
{
  DRAMSim::TransactionCompleteCB *read_cb = new DRAMSim::Callback<Cache, void, unsigned, uint64_t, uint64_t>(this, &Cache::mem_read_complete);
  DRAMSim::TransactionCompleteCB *write_cb = new DRAMSim::Callback<Cache, void, unsigned, uint64_t, uint64_t>(this, &Cache::mem_write_complete);

  this->mem_channel = mem;
  mem_channel->RegisterCallbacks(read_cb, write_cb, empty_cb);
}


RequestPtr Cache::access(cache_cmd_t cmd, maddr_t addr, void (*ready_cb)(Request*))
{
  Block *blk; 
  int cmd_idx = cmd-1;
  
  if (!cmd)
    return NULL;

  // generate new request
  inReqs.push_back(make_shared<Request>(cmd, addr, ready_cb));
  

  return inReqs.back();
}

bool Cache::lookup(Request *req) 
{
  bool hit = false;
  int cmd_idx = req->cmd -1;  
  auto blk = this->probe(req->addr);
  this->access_count++;
  
  // is this a hit or miss?
  if (blk) {
    hit_counts[cmd_idx]++;
    hit = true;
  }
  else {
    miss_counts[cmd_idx]++;
    blk = handle_miss(req->cmd, req->addr);
  }

  // enforce LRU ordering
  lru_reorder(blk);

  return hit;
}


/*

  on cache hit: does this satisfy incoming request (in upReqs)?
                   yes:  
		   no:
  
 */


/*
  process eventQ
  update now
*/


void cache_data_ready(Request *req)
{
  // empty
}

void no_action(Request *req)
{
  // empty
}

void Cache::update(void)
{
  // find the next incoming request
  auto next_req_it = inReqs.begin();
  if (next_req_it != inReqs.end()) {
    RequestPtr req(*next_req_it);

    if (!req->issued) {
      auto ev = new LookupEvent(now + latency - 1, req.get());
      req->issued = true;
      if (verbose)
	std::cout << "Adding to event queue: " << req->addr << std::endl;
      eventQ.push(ev);
    }
  }

  //std::cout << "eventQ.size() = " << eventQ.size() << std::endl;

  while (!eventQ.empty() && eventQ.top()->when <= now) {
    RequestPtr req(eventQ.top()->req);

    if (lookup(req.get()))  {
      if (verbose)
	std::cout << "Cache hit on req= " << req << std::endl;
      // this was a hit
      req->ready_cb(req.get());
      req->complete = true;
    }
    else {
      // this was a miss
      if (verbose)
	std::cout << "Cache miss on req= " << req << std::endl;
      if (next_level) {
	RequestPtr next_req(next_level->access(req->cmd, req->addr, cache_data_ready));
	next_req->prev = req;
	req->next = next_req;
	outReqs.push_back(std::move(next_req));
      }
      else {
	// no next level cache...send request to DRAM
	RequestPtr mem_req = make_shared<Request>(req->cmd, req->addr, no_action);

	// a better way to do this would be to create a new mem channel transaction
	// and then use the resultant ID to do tracking
	mem_channel->addTransaction((req->cmd == CACHE_CMD_WRITE), req->addr);

	mem_req->toMemory = true;
	mem_req->prev = req;
	req->next = mem_req;
	outReqs.push_back(mem_req);
	memReqs.push_back(mem_req);  // <--- should make a copy, right?
      }
    }
    eventQ.pop();
  }


  // find any completed requests
  auto out_req_it = outReqs.begin(); 
  while (out_req_it != outReqs.end()) {
    RequestPtr req(*out_req_it);
    if (req->complete) {
      req->prev->complete = true;
      req->prev->ready_cb(req.get());
      out_req_it =  outReqs.erase(out_req_it);
    }
    else
      ++out_req_it;
  }

  // find any completed requests
  auto req_in_it = inReqs.begin(); 
  while (req_in_it != inReqs.end()) {
    RequestPtr req(*req_in_it);
    if (req->complete) {
      req_in_it =  inReqs.erase(req_in_it);
    }
    else
      ++req_in_it;
  }

  // advance time
  this->now++;
}

int Cache::get_accesses(void)
{
  return access_count;
}

int Cache::get_overall_misses(void)
{
  int i, sum = 0;

  for(i = 0; i < CACHE_CMD_COUNT; i++)
    sum += this->miss_counts[i];

  return sum;
}

int Cache::get_overall_hits(void)
{
  int i, sum = 0;

  for(i = 0; i < CACHE_CMD_COUNT; i++)
    sum += this->hit_counts[i];

  return sum;
}

void Cache::dump_stats(void) 
{
  std::cout << dec;
  std::cout << name << ".accesses\t\t" << get_accesses() << std::endl;
  std::cout << name << ".hits\t\t" << get_overall_hits() << std::endl;
  std::cout << name << ".misses\t\t" << get_overall_misses() << std::endl;
  std::cout << name << ".miss_rate\t\t" << ((double) get_overall_misses()) / get_accesses() << std::endl;
}
