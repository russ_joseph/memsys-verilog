#ifndef CACHE_H
#define CACHE_H

#include <stdint.h>
#include <string>
#include <vector>
#include <map>
#include <queue>
#include <iostream>
#include <memory>

#include <DRAMSim.h>


using namespace std;

extern bool verbose;


typedef uint32_t maddr_t;
typedef uint64_t tick_t;

typedef enum {
  CACHE_CMD_NONE = 0,
  CACHE_CMD_READ,
  CACHE_CMD_WRITE,
  CACHE_CMD_FETCH,
  CACHE_CMD_COUNT
} cache_cmd_t;

class Cache;

class Request;
typedef std::shared_ptr<Request> RequestPtr;

class Request {
 public:
  cache_cmd_t cmd;
  maddr_t addr;
  bool issued;
  bool complete;
  bool toMemory;
  void (*ready_cb)(Request*);
  RequestPtr prev, next;
  
 Request(cache_cmd_t _cmd, maddr_t _addr, void (*_ready_cb)(Request*)) : cmd(_cmd), addr(_addr), issued(false), complete(false), toMemory(false), ready_cb(_ready_cb), prev(NULL), next(NULL) {}

};



class Event {
 public:
  tick_t when;
  Event(tick_t _when) : when(_when) {}
};

class LookupEvent : public Event {
 public:
  RequestPtr req;
  LookupEvent(tick_t when, Request *_req) : Event(when), req(_req) {}
};


class Block {
 public:
    bool valid;
    bool dirty;
    maddr_t block_addr;

    Block() {}
};


class Cache {

 public:
  Cache(std::string name, int assoc, int capacity, int block_size, int latency);
  ~Cache();

  std::string name;
 private:


  int now;
  int assoc;
  int n_sets;
  int block_size;   // number of bytes in each block
  int block_shift;  // used to calculate frame
  int block_mask;   // used to calculate block address
  maddr_t set_mask;   // access set index
  maddr_t offset_mask;  // access byte offset into block
  int latency;
  int access_count;
  vector<int> hit_counts;
  vector<int> miss_counts;
  priority_queue<LookupEvent*> eventQ;


  vector<vector<Block*> > blocks;

  Block* probe(maddr_t addr);
  inline int findset(maddr_t addr) 
  {
    return (addr >> this->block_shift) & this->set_mask; 
  }
  
  void lru_reorder(Block *blk);
  Block* handle_miss(cache_cmd_t cmd, maddr_t addr);

  std::vector<RequestPtr> inReqs, outReqs, memReqs;

 private: 
  RequestPtr find_mem_request(uint64_t address);

 public:
  Cache *next_level;
  DRAMSim::MultiChannelMemorySystem *mem_channel;
  void connect_next(Cache *lower);
  void connect_dram(DRAMSim::MultiChannelMemorySystem *mem);


  void mem_read_complete(unsigned id, uint64_t address, uint64_t clock_cycle);
  void mem_write_complete(unsigned id, uint64_t address, uint64_t clock_cycle);

  RequestPtr access(cache_cmd_t cmd, maddr_t addr, void (*ready_cb)(Request*));
  bool lookup(Request*);
  void update(void);
  void dump_stats(void);

  int get_accesses(void);
  int get_overall_misses(void);
  int get_overall_hits(void);
};

#endif
