#include <stdio.h>
#include <stdlib.h>
#include "cache.h"


int main(int argc, char **argv)
{
  cache_t *tc, *l2;
  int capacity, assoc, block_size, lat_sum = 0;
  int idx;
  dram_t *main_mem;

  tick_t now = 0;

  if (argc != 2 || sscanf(argv[1], "%d:%d:%d", &capacity, &assoc,  &block_size) != 3) {
    fprintf(stderr, "%s: invalid arguments\n", argv[0]);
    exit(-1);
  }
  
  main_mem = dram_alloc("main_mem", 200);
  l2 = cache_alloc("l2", 4 * assoc, 4 * capacity, block_size, 20, NULL, main_mem);
  tc = cache_alloc("dummy", assoc, capacity, block_size, 1, l2, NULL);


  lat_sum += cache_access(tc, 0, CACHE_CMD_READ, now);
  lat_sum += cache_access(tc, 128*64, CACHE_CMD_READ, now);
  lat_sum += cache_access(tc, 0, CACHE_CMD_READ, now);
  lat_sum += cache_access(tc, 128*64, CACHE_CMD_READ, now);
  lat_sum += cache_access(tc, 2*128*64, CACHE_CMD_READ, now);
  lat_sum += cache_access(tc, 128*64, CACHE_CMD_READ, now);

  
  fprintf(stdout, "miss_rate = %f\n", ((double) cache_overall_misses(tc) / tc->access_count));
  fprintf(stdout, "l2_miss_rate = %f\n", ((double) cache_overall_misses(l2) / l2->access_count));
  fprintf(stdout, "avg_latency = %f\n",  (double)(lat_sum) / tc->access_count);
}
