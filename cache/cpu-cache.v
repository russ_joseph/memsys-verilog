module top;
   integer cycle;
   initial begin
      cycle = $icache_access(32'h4000, 3, 0);
      $display("icache cycle = %d", cycle);
      cycle = $dcache_access(32'h8000, 1, 0);
      $display("dcache cycle = %d", cycle);
   end

endmodule // top
